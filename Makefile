all: reflex 3_4_3.4d

3_3_3.4d 3_4_3.4d 4_3_3.4d 5_3_3.4d: Polychoron
	./Polychoron

Polychoron: Polychoron.hs Vector.hs .cabal-sandbox
	cabal exec -- ghc -O2 -Wall --make Polychoron.hs

reflex: reflex.hs Ball.hs Vector.hs Snapshot.hs .cabal-sandbox
	cabal exec -- ghc -O2 -Wall --make reflex.hs

.cabal-sandbox:
	cabal sandbox init
	cabal install GLUT
