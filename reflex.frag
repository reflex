#version 330 core
#info Reflex (c) 2009, 2022 Claude Heiland-Allen <https://mathr.co.uk/blog/2022-02-13_reflex_reboot.html>
#define providesColor
#include "MathUtils.frag"
#include "DE-Raytracer.frag"

uniform float time;

#group Reflex

const float pi = 3.141592653;

/*
Four dimensional cross product of three vectors.
*/

vec4 cross4(vec4 u, vec4 v, vec4 w)
{
  mat3 m0 = mat3(u[1], u[2], u[3], v[1], v[2], v[3], w[1], w[2], w[3]);
  mat3 m1 = mat3(u[0], u[2], u[3], v[0], v[2], v[3], w[0], w[2], w[3]);
  mat3 m2 = mat3(u[0], u[1], u[3], v[0], v[1], v[3], w[0], w[1], w[3]);
  mat3 m3 = mat3(u[0], u[1], u[2], v[0], v[1], v[2], w[0], w[1], w[2]);
  return vec4(determinant(m0), -determinant(m1), determinant(m2), -determinant(m3));
}

/*
Magic delta determinant used for calculating radii of sub-polytopes.  These
radii can be used to find the angles subtended by sub-polytopes, for example.
*/

float delta(void)
{
  return 1.0;
}

float delta(int p)
{
  float s = sin(pi / float(p));
  return s * s;
}

float delta(ivec2 p)
{
  float c = cos(pi / float(p.x));
  return delta(p.y) - delta() * c * c;
}

float delta(ivec3 p)
{
  float c = cos(pi / float(p.x));
  return delta(p.yz) - delta(p.z) * c * c;
}

/*
Compute the squared radius of a sub-polytope.
*/

float radius2_0()
{
  return 1.0;
}

float radius2_0(int p)
{
  return delta() / delta(p);
}

float radius2_0(ivec2 p)
{
  return delta(p.y) / delta(p);
}

float radius2_0(ivec3 p)
{
  return delta(p.yz) / delta(p);
}

float radius2_j(int j, int p)
{
  switch (j)
  {
    case 0: return radius2_0(p);
    case 1: return radius2_0(p) - radius2_0();
  }
  return 0.0;
}

float radius2_j(int j, ivec2 p)
{
  switch (j)
  {
    case 0: return radius2_0(p);
    case 1: return radius2_0(p) - radius2_0();
    case 2: return radius2_0(p) - radius2_0(p.x);
  }
  return 0.0;
}

float radius2_j(int j, ivec3 p)
{
  switch (j)
  {
    case 0: return radius2_0(p);
    case 1: return radius2_0(p) - radius2_0();
    case 2: return radius2_0(p) - radius2_0(p.x);
    case 3: return radius2_0(p) - radius2_0(p.xy);
  }
  return 0.0;
}

float radius(int j, ivec2 p)
{
  return sqrt(radius2_j(j, p));
}

float radius(int j, ivec3 p)
{
  return sqrt(radius2_j(j, p));
}

/*
Compute the fundamental region of the regular polytope {p,q,r}.

The result is the normal vectors of the bounding planes of the
fundamental region.
*/

mat4 fr4(ivec3 pqr)
{
  float r0 = radius(0, pqr);
  float r1 = radius(1, pqr);
  float r2 = radius(2, pqr);
  float r3 = radius(3, pqr);
  float c1 = r1 / r0;
  float c2 = r2 / r1;
  float c3 = r3 / r2;
  float s1 = sqrt(1.0 - c1 * c1);
  float s2 = sqrt(1.0 - c2 * c2);
  float s3 = sqrt(1.0 - c3 * c3);
  mat4 m1 = mat4(c1, s1, 0, 0, -s1, c1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
  mat4 m2 = mat4(c2, 0, s2, 0, 0, 1, 0, 0, -s2, 0, c2, 0, 0, 0, 0, 1);
  mat4 m3 = mat4(c3, 0, 0, s3, 0, 1, 0, 0, 0, 0, 1, 0, -s3, 0, 0, c3);
  vec4 v0 = vec4(1, 0, 0, 0);
  vec4 v1 = m1 * v0;
  vec4 v2 = m1 * m2 * v0;
  vec4 v3 = m1 * m2 * m3 * v0;
  return (mat4
    (  normalize(cross4(v1, v2, v3))
    , -normalize(cross4(v0, v2, v3))
    ,  normalize(cross4(v0, v1, v3))
    , -normalize(cross4(v0, v1, v2))
    ));
}

mat3 fr3(ivec2 pq)
{
  float r0 = radius(0, pq);
  float r1 = radius(1, pq);
  float r2 = radius(2, pq);
  float c1 = r1 / r0;
  float c2 = r2 / r1;
  float s1 = sqrt(1.0 - c1 * c1);
  float s2 = sqrt(1.0 - c2 * c2);
  mat3 m1 = mat3(c1, s1, 0, -s1, c1, 0, 0, 0, 1);
  mat3 m2 = mat3(c2, 0, s2, 0, 1, 0, -s2, 0, c2);
  vec3 v0 = vec3(1, 0, 0);
  vec3 v1 = m1 * v0;
  vec3 v2 = m1 * m2 * v0;
  return mat3
    (  normalize(cross(v1, v2))
    , -normalize(cross(v0, v2))
    ,  normalize(cross(v0, v1))
    );
}

// Dimension
uniform int Dimension; slider[3,3,4]

// Symmetry group type. 3D: 0 = tetrahedron, 1 = cube, 2 = octahedron, 3 = dodecahedron, 4 = icosahedron; 4D: 0 = 5-cell, 1 = 8-cell, 2 = 16-cell, 3 = 120-cell, 4 = 600-cell, 5 = 24-cell
uniform int Symmetry; slider[0,2,5]
ivec2 Group3D =
  Symmetry == 0 ? ivec2(3, 3) :
  Symmetry == 1 ? ivec2(4, 3) :
  Symmetry == 2 ? ivec2(3, 4) :
  Symmetry == 3 ? ivec2(5, 3) :
                  ivec2(3, 5) ;
ivec3 Group4D =
  Symmetry == 0 ? ivec3(3, 3, 3) :
  Symmetry == 1 ? ivec3(4, 3, 3) :
  Symmetry == 2 ? ivec3(3, 3, 4) :
  Symmetry == 3 ? ivec3(5, 3, 3) :
  Symmetry == 4 ? ivec3(3, 3, 5) :
                  ivec3(3, 4, 3) ;
mat3 FR3 = fr3(Group3D);
mat4 FR4 = fr4(Group4D);

// Barycenter
uniform float BX; slider[0,0.5,1]
uniform float BY; slider[0,0.5,1]
uniform float BZ; slider[0,0.5,1]
uniform float BW; slider[0,0.5,1]

uniform bool P01; checkbox[false]
uniform bool P02; checkbox[false]
uniform bool P03; checkbox[false]
uniform bool P12; checkbox[false]
uniform bool P13; checkbox[false]
uniform bool P23; checkbox[false]

vec3 c = vec3(1);

vec4 unstereo(vec3 pos)
{
  float r = length(pos);
  return vec4(2.0 * pos, 1.0 - r * r) / (1.0 + r * r);
}

vec3 stereo(vec4 pos)
{
  pos = normalize(pos);
  return pos.xyz / (1 - pos.w);
}

float sdistance(vec4 a, vec4 b)
{
  return distance(stereo(a), stereo(b));
}

float poly4(vec4 r)
{
  float de = 1.0 / 0.0;
  for (int i = 0, j = 0; i < 4 && j < 600; ++j)
  {
    if (dot(r, FR4[i]) < 0)
    {
      r = reflect(r, FR4[i]);
      i = 0;
    }
    else
    {
      i++;
    }
  }
  vec4 s = inverse(transpose(FR4)) * vec4(BX, BY, BZ, BW);
  vec4 t = r;
/*
  float d0 = sdistance(s, t - FR4[0] * dot(t, FR4[0])) - 0.025; vec3 c0 = vec3(1, 0.5, 0);
  float d1 = sdistance(s, t - FR4[1] * dot(t, FR4[1])) - 0.025; vec3 c1 = vec3(0, 1, 0.5);
  float d2 = sdistance(s, t - FR4[2] * dot(t, FR4[2])) - 0.025; vec3 c2 = vec3(0.5, 0, 1);
  float d3 = sdistance(s, t - FR4[3] * dot(t, FR4[3])) - 0.025; vec3 c3 = vec3(0.5, 1, 0);
*/
  vec4 p4 = normalize(cross4(FR4[0], FR4[1], t)); float d4 = sdistance(s, s  - p4 * dot(s, p4)) - 0.01; vec3 c4 = vec3(1, 0, 0);
  vec4 p5 = normalize(cross4(FR4[0], FR4[2], t)); float d5 = sdistance(s, s  - p5 * dot(s, p5)) - 0.01; vec3 c5 = vec3(1, 1, 0);
  vec4 p6 = normalize(cross4(FR4[0], FR4[3], t)); float d6 = sdistance(s, s  - p6 * dot(s, p6)) - 0.01; vec3 c6 = vec3(0, 1, 0);
  vec4 p7 = normalize(cross4(FR4[1], FR4[2], t)); float d7 = sdistance(s, s  - p7 * dot(s, p7)) - 0.01; vec3 c7 = vec3(0, 1, 1);
  vec4 p8 = normalize(cross4(FR4[1], FR4[3], t)); float d8 = sdistance(s, s  - p8 * dot(s, p8)) - 0.01; vec3 c8 = vec3(1, 0.5, 0);
  vec4 p9 = normalize(cross4(FR4[2], FR4[3], t)); float d9 = sdistance(s, s  - p9 * dot(s, p9)) - 0.01; vec3 c9 = vec3(1, 0, 1);
/*
  if (d0 < de) { de = d0; c = c0; }
  if (d1 < de) { de = d1; c = c1; }
  if (d2 < de) { de = d2; c = c2; }
  if (d3 < de) { de = d3; c = c3; }
*/
  if (P01) if (d4 < de) { de = d4; c = c4; }
  if (P02) if (d5 < de) { de = d5; c = c5; }
  if (P03) if (d6 < de) { de = d6; c = c6; }
  if (P12) if (d7 < de) { de = d7; c = c7; }
  if (P13) if (d8 < de) { de = d8; c = c8; }
  if (P23) if (d9 < de) { de = d9; c = c9; }
  return de;
}


float poly3(vec3 r)
{
  float de = 1.0 / 0.0;
  for (int i = 0, j = 0; i < 3 && j < 100; ++j)
  {
    if (dot(r, FR3[i]) < 0)
    {
      r = reflect(r, FR3[i]);
      i = 0;
    }
    else
    {
      i++;
    }
  }
  vec3 t = r - inverse(transpose(FR3)) * vec3(BX, BY, BZ);
  float dv = length(t) - 0.03; vec3 cv = vec3(1, 1, 0);
  float d0 = length(t - FR3[0] * dot(t, FR3[0])) - 0.1; vec3 c0 = vec3(1, 0, 0);
  float d1 = length(t - FR3[1] * dot(t, FR3[1])) - 0.1; vec3 c1 = vec3(0, 1, 0);
  float d2 = length(t - FR3[2] * dot(t, FR3[2])) - 0.1; vec3 c2 = vec3(0, 0, 1);
  if (dv < de) { de = dv; c = cv; }
  if (d0 < de) { de = d0; c = c0; }
  if (d1 < de) { de = d1; c = c1; }
  if (d2 < de) { de = d2; c = c2; }
  return de;
}

float DE(vec3 pos)
{
  float t = 2.0 * pi * time / 12.0;
  if (Dimension == 3)
  {
    pos.xy *= mat2(cos(t), sin(t), -sin(t), cos(t));
    return poly3(pos);
  }
  else
  {
    vec4 z = unstereo(pos);
    z.xy *=  mat2(cos(t), sin(t), -sin(t), cos(t));
    z.zw *=  mat2(cos(t), sin(t), -sin(t), cos(t));
    return poly4(z);
  }
}

vec3 baseColor(vec3 pos, vec3 n)
{
  DE(pos);
  float t = 2.0 * pi * time / 12.0;
  vec4 z = unstereo(pos);
  z.xy *=  mat2(cos(t), sin(t), -sin(t), cos(t));
  if (Dimension == 4)
  {
    z.zw *=  mat2(cos(t), sin(t), -sin(t), cos(t));
  }
  z *= 4.0;
  z -= floor(z);
  bool l = length(z - vec4(0.5)) < 0.5;
  z += vec4(0.5);
  z -= floor(z);
  bool d = length(z - vec4(0.5)) < 0.5;
  return vec3(l ? 1 : d ? 0.25 : 0.5) * c;
}



#preset 24-Cell
FOV = 0.4
Eye = 2.5,0,0.5
Target = 0,0,0
Up = 0,1,0
EquiRectangular = false
AutoFocus = false
FocalPlane = 2
Aperture = 0.01
Gamma = 2
ToneMapping = 1
Exposure = 2
Brightness = 2
Contrast = 1
AvgLumin = 0.5,0.5,0.5
Saturation = 1
LumCoeff = 0.212500006,0.715399981,0.0720999986
Hue = 0
GaussianWeight = 1
AntiAliasScale = 2
DepthToAlpha = false
ShowDepth = false
DepthMagnitude = 1
Detail = -5
DetailAO = -0.5
FudgeFactor = 0.125
MaxDistance = 5000
MaxRaySteps = 1241
Dither = 0.5
NormalBackStep = 1
AO = 0,0,0,0.7
Specular = 0.1
SpecularExp = 35
SpecularMax = 50
SpotLight = 0.6,0.756862745,0.945098039,1
SpotLightDir = -0.2,-0.8
CamLight = 0.560784314,0.941176471,0.643137255,0.1
CamLightMin = 0
Glow = 1,1,1,0
GlowMax = 0
Fog = 0.7
HardShadow = 0
ShadowSoft = 0 Locked
QualityShadows = false Locked
Reflection = 0
DebugSun = false
BaseColor = 1,1,1
OrbitStrength = 0.75
X = 0.5,0.600000024,0.600000024,0.699999988
Y = 1,0.600000024,0,0.400000006
Z = 0.800000012,0.779999971,1,0.5
R = 0.400000006,0.699999988,1,0.119999997
BackgroundColor = 0,0,0
GradientBackground = 0.3
CycleColors = false
Cycles = 1.1
EnableFloor = false
FloorNormal = 0,0,1
FloorHeight = 0
FloorColor = 1,1,1
Dimension = 4
Symmetry = 5
P01 = false
P02 = true
P03 = false
P12 = true
P13 = false
P23 = false
BX = 1
BY = 0.25
BZ = 0
BW = 0.0625
#endpreset
