module Ball where

import Vector

ball :: R -> V3 -> M4
ball rr v =
  let r = sqrt $ v`dot`v
      d = sqrt $ rr*rr + r*r
      c = rr / d
      s =  r / d
      V3 x y z = if r /= 0 then norm v else V3 0 0 0
  in M4
      (1-x*x*(1-c)) (-(1-c)*x*y)  (-(1-c)*x*z)  (s*x)
      (-(1-c)*x*y)  (1-y*y*(1-c)) (-(1-c)*y*z)  (s*y)
      (-(1-c)*x*z)  (-(1-c)*y*z)  (1-z*z*(1-c)) (s*z)
      (-s*x)        (-s*y)        (-s*z)        (c)
