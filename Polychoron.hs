module Main(main) where

import Control.Monad(liftM2)
import Data.List(deleteFirstsBy, nubBy)
import System.IO(withBinaryFile, hPutBuf, IOMode(WriteMode))
import Foreign.Storable(sizeOf)
import Foreign.Marshal.Array(withArrayLen)

import Vector

{-

Compute the fundamental region of the regular polytope {p,q,r}.

The result is the normal vectors of the bounding planes of the
fundamental region.

-}
fr4 :: Int -> Int -> Int -> [V4]
fr4 p q r =
  let v0 = V4 1 0 0 0
      v1 = m1 ^^*^ v0
      v2 = (m1 ^^*^^ m2) ^^*^ v0
      v3 = ((m1 ^^*^^ m2) ^^*^^ m3) ^^*^ v0
      m1 = M4 c1 s1 0 0   (-s1) c1 0 0   0 0 1 0   0 0 0 1
      m2 = M4 c2 0 s2 0   0 1 0 0   (-s2) 0 c2 0   0 0 0 1
      m3 = M4 c3 0 0 s3   0 1 0 0   0 0 1 0   (-s3) 0 0 c3
      c1 = r1 / r0
      c2 = r2 / r1
      c3 = r3 / r2
      s1 = sqrt (1 - c1*c1)
      s2 = sqrt (1 - c2*c2)
      s3 = sqrt (1 - c3*c3)
      r0 = radius 0 [p,q,r]
      r1 = radius 1 [p,q,r]
      r2 = radius 2 [p,q,r]
      r3 = radius 3 [p,q,r]
  in [ norm $ cross4 v1 v2 v3
     , norm $ cross4 v0 v2 v3 ^* (-1)
     , norm $ cross4 v0 v1 v3
     , norm $ cross4 v0 v1 v2 ^* (-1) ]

{-

Magic delta determinant used for calculating radii of sub-polytopes.  These
radii can be used to find the angles subtended by sub-polytopes, for example.

-}

delta :: [Int] -> R
delta [] = 1
delta [p] = let s = sin (pi / fromIntegral p) in s * s
delta (p:q:rs) = let c = cos (pi / fromIntegral p) in delta (q:rs) - delta rs * c * c

{-

Compute the squared radius of a sub-polytope.

-}

radius2 :: Int -> [Int] -> R
radius2 0 [] = 1
radius2 0 (p:qs) = delta qs / delta (p:qs)
radius2 j ps = radius2 0 ps - radius2 0 (take (j-1) ps) 

{-

Non-squared radius.

-}

radius :: Int -> [Int] -> R
radius n ps = sqrt $ radius2 n ps

{-

Compute the transitive closure of a set of generators.

In closure' gs ms ns:
  gs : the generators
  ms : the accumulated closure
  ns : the new elements added last iteration

-}

closure :: (Approx m, M m) => [m] -> [m]
closure gs = closure' gs gs gs

closure' :: (Approx m, M m) => [m] -> [m] -> [m] -> [m]
closure' gs ms ns =
  let new = (deleteFirstsBy (~=~) . nubBy (~=~) . liftM2 (^^*^^) gs $ ns) ms
  in if null new then ms else closure' gs (ms ++ new) new

{-

A polytope is represented by the matrices that make up its symmetry group.

-}

polychoron :: Int -> Int -> Int -> [M4]
polychoron p q r = closure . map reflector $ fr4 p q r

main = do
  mapM_ write4D [(3, 3, 3), (4, 3, 3), (3, 4, 3), (5, 3, 3)]
  where
    write4D (p, q, r) =
      let name = show p ++ "_" ++ show q ++ "_" ++ show r ++ ".4d"
      in  withBinaryFile name WriteMode $ \h ->
            withArrayLen (concatMap toList $ polychoron p q r) $ \n ptr -> do
              putStrLn $ name ++ " (" ++ show n ++ ")"
              hPutBuf h ptr (n * sizeOf (undefined::R))
    toList (M4 a b c d e f g h i j k l m n o p) = [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p]
