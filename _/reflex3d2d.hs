module Main where

import Control.Monad(forever, when)
import Data.IORef(IORef, newIORef, atomicModifyIORef)
import Control.Concurrent(forkIO, yield)
import System.IO(hGetBuf, withBinaryFile, IOMode(ReadMode), hSetBuffering, BufferMode(LineBuffering), stdin)
import Foreign.Marshal.Alloc(allocaBytes)
import Foreign.Marshal.Array(peekArray)
import Foreign.Storable(sizeOf)
import Graphics.UI.GLUT hiding(Point, project)

import Vector hiding (i, o)

fr3 :: Int -> Int -> [V3]
fr3 p q =
  let v0 = V3 1 0 0
      v1 = m1 ^^*^ v0
      v2 = (m1 ^^*^^ m2) ^^*^ v0
      m1 = M3 c1 s1 0   (-s1) c1 0   0 0 1
      m2 = M3 c2 0 s2   0 1 0   (-s2) 0 c2
      c1 = r1 / r0
      c2 = r2 / r1
      s1 = sqrt (1 - c1*c1)
      s2 = sqrt (1 - c2*c2)
      r0 = radius 0 [p,q]
      r1 = radius 1 [p,q]
      r2 = radius 2 [p,q]
  in [ norm $ cross3 v1 v2
     , norm $ cross3 v0 v2 ^* (-1)
     , norm $ cross3 v0 v1
     , v0, v1, v2 ]

delta :: [Int] -> R
delta [] = 1
delta [p] = let s = sin (pi / fromIntegral p) in s * s
delta (p:q:rs) = let c = cos (pi / fromIntegral p) in delta (q:rs) - delta rs * c * c

radius2 :: Int -> [Int] -> R
radius2 0 [] = 1
radius2 0 (p:qs) = delta qs / delta (p:qs)
radius2 j ps = radius2 0 ps - radius2 0 (take (j-1) ps) 

radius :: Int -> [Int] -> R
radius n ps = sqrt $ radius2 n ps

dropV :: V v => v -> v -> v
dropV u v =
  let k = u`dot`v / u`dot`u
  in v ^-^ (k *^ u)

data Figure3 = Figure3 !V3 !V3 !V3 !V3 !V3 !V3 !V3 !Bool

instance Approx Figure3 where
  Figure3 p a b c _ _ _ _ ~=~ Figure3 q e f g _ _ _ _ = and $ zipWith (~=~) [p,a,b,c] [q,e,f,g]

initialP3 :: [V3] -> V3 -> V3
initialP3 [V3 a11 a12 a13, V3 a21 a22 a23, V3 a31 a32 a33, _, _, _] v =
  let m = M3 a11 a12 a13 a21 a22 a23 a31 a32 a33
      u = norm $ inv m ^^*^ v
  in u
initialP3 _ _ = error "initialP3"

initialF3 :: [V3] -> V3 -> Figure3
initialF3 [l0,l1,l2,v0,_,v2] p = Figure3 p a b c d e f True
  where (a, b, c) = (dropV l0 p , dropV l1 p , dropV l2 p)
        (d, e, f) = (v2 , v0 , (dropV l0 . dropV l2) p)
initialF3 _ _ = error "initialF3"

transformF3 :: M3 -> Figure3 -> Figure3
transformF3 m (Figure3 p a b c d e f z) = Figure3 (m ^^*^ p) (m ^^*^ a) (m ^^*^ b) (m ^^*^ c) (m ^^*^ d) (m ^^*^ e) (m ^^*^ f) (if det m > 0 then z else not z)

transformsF3 :: M3 -> [Figure3] -> [Figure3]
transformsF3 m fs = map (m `transformF3`) fs

polyhedronF :: [M3] -> Figure3 -> [Figure3]
polyhedronF ms f = map (`transformF3` f) ms


quality = 6

drawFigure3 :: (Color3 R, Color3 R, Color3 R) -> (Bool, Bool, Bool) -> Figure3 -> IO ()
drawFigure3 (ac,bc,cc) (av,bv,cv) (Figure3 p a b c d e f z) = do
  let seg p1 p2 = let V2 x1 y1 = project p1
                      V2 x2 y2 = project p2
                  in vertex (Vertex2 x1 y1) >> vertex (Vertex2 x2 y2)
      lin p1 p2 n = let dir = (p2 ^-^ p1 ^/ fromIntegral n) in map (\i -> norm $ p1 ^+^ (fromIntegral i *^ dir)) [0 .. n :: Int]
      line p1 p2 = let ps = lin p1 p2 quality in sequence_ $ zipWith seg ps (tail ps)
  when av $ color ac >> line p a >> line p d
  when bv $ color bc >> line p b >> line p e
  when cv $ color cc >> line p c >> line p f

drawFigures3 :: (Color3 R, Color3 R, Color3 R) -> (Bool, Bool, Bool) -> [Figure3] -> IO ()
drawFigures3 cs v fs = renderPrimitive Lines $ mapM_ (drawFigure3 cs v) fs


project :: V3 -> V2
project p = let (V3 x y z) = norm p in let k = 0.5 / (1 - z) in V2 (k*x) (k*y)

fillFigure3 :: (Color3 R, Color3 R, Color3 R) -> (Bool, Bool, Bool) -> Figure3 -> IO ()
fillFigure3 (ac,bc,cc) (av,bv,cv) (Figure3 p a b c d e f parity) = do
  when av $ color ac >> triangle' p a d >> triangle' p d b
  when bv $ color bc >> triangle' p b e >> triangle' p e c
  when cv $ color cc >> triangle' p c f >> triangle' p f a
  where
    triangle' p1 p2 p3 = triangle parity quality p1 p2 p3

clockwise :: V2 -> V2 -> V2 -> Bool
clockwise (V2 ax ay) (V2 bx by) (V2 cx cy) = e1x * e2y - e1y * e2x >= 0
  where
    (e1x, e1y) = (ax - bx, ay - by)
    (e2x, e2y) = (cx - bx, cy - by)

triangle :: Bool -> Int -> V3 -> V3 -> V3 -> IO ()
triangle cw 0 p1 p2 p3 = do
  let v1@(V2 x1 y1) = project p1
      v2@(V2 x2 y2) = project p2
      v3@(V2 x3 y3) = project p3
  when (clockwise v1 v2 v3 == cw) $ do
    vertex (Vertex2 x1 y1)
    vertex (Vertex2 x2 y2)
    vertex (Vertex2 x3 y3)

triangle cw n p1 p2 p3 = do
  let q1 = 0.5 *^ (p2 ^+^ p3)
      q2 = 0.5 *^ (p3 ^+^ p1)
      q3 = 0.5 *^ (p1 ^+^ p2)
  triangle (not cw) (n-1) p1 q2 q3
  triangle (not cw) (n-1) q1 p2 q3
  triangle (not cw) (n-1) q1 q2 p3
  triangle cw (n-1) q1 q2 q3

fillFigures3 :: (Color3 R, Color3 R, Color3 R) -> (Bool, Bool, Bool) -> [Figure3] -> IO ()
fillFigures3 cs v fs = renderPrimitive Triangles $ mapM_ (fillFigure3 cs v) fs

data State = State { sV :: Size, sVis :: (Bool, Bool, Bool), sP0 :: Point, sP :: Polytope, sFrame :: Int }

reshape :: IORef State -> Size -> IO ()
reshape r v = atomicModifyIORef r $ \s -> (s { sV = v }, ())

timer :: Timeout -> IORef State -> IO ()
timer t r = do
  addTimerCallback t $ timer t r
  postRedisplay Nothing

data Polytope = Polytope [M3] [V3]
data Point = Point V3

red :: Color3 R
red = Color3 ((0.114/0.299)**0.2) 0 0

green :: Color3 R
green = Color3 0 ((0.114/0.587)**0.2) 0

blue :: Color3 R
blue  = Color3 0 0 1

display :: IORef State -> IO ()
display ref = do
  s <- atomicModifyIORef ref $ \x -> (x, x)
  let vp@(Size vw vh) = sV s
      w = 2 * fromIntegral vw / fromIntegral vh
      h = 2
  viewport $= (Position 0 0, vp)
  matrixMode $= Projection
  loadIdentity
  ortho (-w) (w) (-h) (h) (-1) 1
  matrixMode $= Modelview 0
  loadIdentity
  clear [ColorBuffer, DepthBuffer]
  depthFunc $= Just Less
  shadeModel $= Smooth
  lineSmooth $= Enabled
  lineWidth $= 2
  let Point p = sP0 s
      Polytope syms fr = sP s
      a1 = fromIntegral (sFrame s) / 100
      a2 = fromIntegral (sFrame s) / 141
      a3 = fromIntegral (sFrame s) / 382
      c1 = cos a1
      s1 = sin a1
      c2 = cos a2
      s2 = sin a2
      c3 = cos a3
      s3 = sin a3
      r1 = M3  c1 s1 0   (-s1) c1 0   0 0 1
      r2 = M3  c2 0 s2   0 1 0   (-s2) 0 c2
      r3 = M3  1 0 0   0 c3 s3   0 (-s3) c3
      r = r1 ^^*^^ r2 ^^*^^ r3
  fillFigures3 (red, green, blue) (sVis s) . transformsF3 r . polyhedronF syms . initialF3 fr . initialP3 fr $ p
  flush
  swapBuffers
  atomicModifyIORef ref $ \s'' -> (s'' { sFrame = sFrame s'' + 1 }, ())

main :: IO ()
main = do
  hSetBuffering stdin LineBuffering
  let v = Size 1024 768
  initialWindowSize $= v
  initialDisplayMode $= [RGBAMode, DoubleBuffered, WithDepthBuffer]
  (_, dims) <- getArgsAndInitialize
  let [p,q] = map read dims
      Just (file, size) = lookup (p,q) [((3,3),("3_3.3d", 24))
                                       ,((4,3),("4_3.3d", 48))
                                       ,((5,3),("5_3.3d", 120))]
      n = 3*3 * size
      b = n * sizeOf (undefined::R)
  matrices <- fmap toMats $ withBinaryFile file ReadMode $ \h -> do
   allocaBytes b $ \ptr -> do
    hGetBuf h ptr b
    peekArray n ptr
  createWindow "reflex"
  sr <- newIORef $ State { sV = v, sVis = (True, True, True), sP0 = Point (V3 1 1 1), sP = Polytope matrices (fr3 p q), sFrame = 500 }
  forkIO . forever $ commands sr
  reshapeCallback $= Just (reshape sr)
  displayCallback $= display sr
  idleCallback $= Just yield
  timer 50 sr
  mainLoop

toMats :: [R] -> [M3]
toMats (a:b:c:d:e:f:g:h:i:zz) = M3 a b c d e f g h i : toMats zz
toMats _ = []

commands :: IORef State -> IO ()
commands r = do
  l <- getLine
  case map read (words l) of
    [la,lb,lc,va,vb,vc] -> atomicModifyIORef r $ \s -> (s { sVis = (va/=0,vb/=0,vc/=0), sP0 = Point (V3 la lb lc) }, ())
    _ -> return ()
  postRedisplay Nothing
