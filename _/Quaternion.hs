module Quaternion where

import Vector(R, M4(M4))

data Q = Q !R !R !R !R

qnorm2 :: Q -> R
qnorm2 (Q a b c d) = a*a + b*b + c*c + d*d

qnorm :: Q -> R
qnorm = sqrt . qnorm2

qconj :: Q -> Q
qconj (Q a b c d) = Q a (-b) (-c) (-d)

qinv :: Q -> Q
qinv q = qconj q `qmuls` (1/qnorm2 q)

qadd :: Q -> Q -> Q
qadd (Q a b c d) (Q x y z w) = Q (a+x) (b+y) (c+z) (d+w)

qmul :: Q -> Q -> Q
qmul (Q q1 q2 q3 q4) (Q p1 p2 p3 p4) = Q r1 r2 r3 r4
 where
  r1 = q1 * p1 - q2 * p2 - q3 * p3 - q4 * p4
  r2 = q2 * p1 + q1 * p2 - q4 * p3 + q3 * p4
  r3 = q3 * p1 + q4 * p2 + q1 * p3 - q2 * p4
  r4 = q4 * p1 - q3 * p2 + q2 * p3 + q1 * p4

qexp :: Q -> Q
qexp (Q w x y z) = Q (exp w * c) (x * s) (y * s) (z * s)
 where
  n = sqrt $ x*x + y*y + z*z
  c = cos n
  s = sin n / n

qlog :: Q -> Q
qlog (Q w x y z) = Q (log qn) (x * sac) (y * sac) (z * sac)
 where
  vn2 = x*x + y*y + z*z
  qn2 = vn2 + w*w
  vn = sqrt vn2
  qn = sqrt qn2
  sac = acos (w / qn) / vn

qmuls :: Q -> R -> Q
qmuls (Q w x y z) f = Q (w*f) (x*f) (y*f) (z*f)

qpows :: Q -> R -> Q
qpows q f = qexp $ qlog q `qmuls` f

qslerp :: Q -> Q -> R -> Q
qslerp p q t = ((q `qmul` qinv p) `qpows` t) `qmul` p

mquat2 :: Q -> Q -> M4
mquat2 (Q q0 q1 q2 q3) (Q p0 p1 p2 p3) = M4
  (  q0*p0 + q1*p1 + q2*p2 + q3*p3)
  (  q1*p0 - q0*p1 - q3*p2 + q2*p3)
  (  q2*p0 + q3*p1 - q0*p2 - q1*p3)
  (  q3*p0 - q2*p1 + q1*p2 - q0*p3)
  (- q1*p0 + q0*p1 - q3*p2 + q2*p3)
  (  q0*p0 + q1*p1 - q2*p2 - q3*p3)
  (- q3*p0 + q2*p1 + q1*p2 - q0*p3)
  (  q2*p0 + q3*p1 + q0*p2 + q1*p3)
  (- q2*p0 + q3*p1 + q0*p2 - q1*p3)
  (  q3*p0 + q2*p1 + q1*p2 + q0*p3)
  (  q0*p0 - q1*p1 + q2*p2 - q3*p3)
  (- q1*p0 - q0*p1 + q3*p2 + q2*p3)
  (- q3*p0 - q2*p1 + q1*p2 + q0*p3)
  (- q2*p0 + q3*p1 - q0*p2 + q1*p3)
  (  q1*p0 + q0*p1 + q3*p2 + q2*p3)
  (  q0*p0 - q1*p1 - q2*p2 + q3*p3)
