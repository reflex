module Main(main) where

import Control.Monad(liftM2)
import Data.List(deleteFirstsBy, nubBy)
import System.IO(withBinaryFile, hPutBuf, IOMode(WriteMode))
import Foreign.Storable(sizeOf)
import Foreign.Marshal.Array(withArrayLen)

import Vector

fr3 :: Int -> Int -> [V3]
fr3 p q =
  let v0 = V3 1 0 0
      v1 = m1 ^^*^ v0
      v2 = (m1 ^^*^^ m2) ^^*^ v0
      m1 = M3 c1 s1 0   (-s1) c1 0   0 0 1
      m2 = M3 c2 0 s2   0 1 0   (-s2) 0 c2
      c1 = r1 / r0
      c2 = r2 / r1
      s1 = sqrt (1 - c1*c1)
      s2 = sqrt (1 - c2*c2)
      r0 = radius 0 [p,q]
      r1 = radius 1 [p,q]
      r2 = radius 2 [p,q]
  in [ norm $ cross3 v1 v2
     , norm $ cross3 v0 v2 ^* (-1)
     , norm $ cross3 v0 v1 ]

delta :: [Int] -> R
delta [] = 1
delta [p] = let s = sin (pi / fromIntegral p) in s * s
delta (p:q:rs) = let c = cos (pi / fromIntegral p) in delta (q:rs) - delta rs * c * c

radius2 :: Int -> [Int] -> R
radius2 0 [] = 1
radius2 0 (p:qs) = delta qs / delta (p:qs)
radius2 j ps = radius2 0 ps - radius2 0 (take (j-1) ps) 

radius :: Int -> [Int] -> R
radius n ps = sqrt $ radius2 n ps

closure :: (Approx m, M m) => [m] -> [m]
closure gs = closure' gs gs gs

closure' :: (Approx m, M m) => [m] -> [m] -> [m] -> [m]
closure' gs ms ns =
  let new = (deleteFirstsBy (~=~) . nubBy (~=~) . liftM2 (^^*^^) gs $ ns) ms
  in if null new then ms else closure' gs (ms ++ new) new

polyhedron :: Int -> Int -> [M3]
polyhedron p q = closure . map reflector $ fr3 p q

main = do
  mapM_ write3D [(3, 3), (4, 3), (5, 3)]
  where
    write3D (p, q) =
      let name = "poly_" ++ show p ++ "_" ++ show q
      in  putStrLn $ name ++ " = " ++ show (map toList . filter ((0 <) . det) $ polyhedron p q) ++ ";"
    toList (M3 a b c d e f g h i) = [[a,b,c],[d,e,f],[g,h,i]]
