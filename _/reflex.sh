#!/bin/bash
DISPLAY=":0.0" pd -stderr -nrt -r 48000 -oss -channels 2 -open reflex.pd 2>&1 |
grep "^reflex: " |
sed "s/^reflex: //g" |
DISPLAY=":0.1" ./reflex |
ppmtoy4m -S 444 -F 25:1 |
y4mscaler -O preset=dvd -O yscale=1/1 |
mpeg2enc -f 8 -q 2 -b 8000 -o reflex.m2v
