#!/bin/bash
# usage: ./ppmtodvd.sh output.m2v < input.ppm
# assumes: input ppm stream is 788x576
ppmtoy4m -S 444 -F 25:1 |
y4mscaler -O preset=dvd -O yscale=1/1 |
mpeg2enc -f 8 -q 2 -b 8000 -P -R 2 -o "$1"
