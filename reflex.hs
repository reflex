module Main(main) where

import Control.Monad(liftM2, forever, when)
import Data.List(deleteFirstsBy, nubBy, maximumBy)
import Data.Ord (comparing)
import Data.IORef(IORef, newIORef, atomicModifyIORef)
import Control.Concurrent(forkIO)
import Control.Applicative
import System.IO(hGetBuf, withBinaryFile, IOMode(ReadMode), hSetBuffering, BufferMode(LineBuffering), stdin, stdout)
import Foreign.Marshal.Alloc(allocaBytes)
import Foreign.Marshal.Array(peekArray, withArray)
import Foreign.Ptr (Ptr, castPtr)
import Foreign.Storable(sizeOf)
import Graphics.UI.GLUT

import Snapshot
import Vector hiding (i, o)
import qualified Vector as V
import Ball

fr4 :: Int -> Int -> Int -> [V4]
fr4 p q r =
  let v0 = V4 1 0 0 0
      v1 = m1 ^^*^ v0
      v2 = (m1 ^^*^^ m2) ^^*^ v0
      v3 = ((m1 ^^*^^ m2) ^^*^^ m3) ^^*^ v0
      m1 = M4 c1 s1 0 0   (-s1) c1 0 0   0 0 1 0   0 0 0 1
      m2 = M4 c2 0 s2 0   0 1 0 0   (-s2) 0 c2 0   0 0 0 1
      m3 = M4 c3 0 0 s3   0 1 0 0   0 0 1 0   (-s3) 0 0 c3
      c1 = r1 / r0
      c2 = r2 / r1
      c3 = r3 / r2
      s1 = sqrt (1 - c1*c1)
      s2 = sqrt (1 - c2*c2)
      s3 = sqrt (1 - c3*c3)
      r0 = radius 0 [p,q,r]
      r1 = radius 1 [p,q,r]
      r2 = radius 2 [p,q,r]
      r3 = radius 3 [p,q,r]
      l0 = norm $ cross4 v1 v2 v3
      l1 = norm $ cross4 v0 v2 v3 ^* (-1)
      l2 = norm $ cross4 v0 v1 v3
      l3 = norm $ cross4 v0 v1 v2 ^* (-1)
  in [l0, l1, l2, l3]

delta :: [Int] -> R
delta [] = 1
delta [p] = let s = sin (pi / fromIntegral p) in s * s
delta (p:q:rs) = let c = cos (pi / fromIntegral p) in delta (q:rs) - delta rs * c * c

radius2 :: Int -> [Int] -> R
radius2 0 [] = 1
radius2 0 (p:qs) = delta qs / delta (p:qs)
radius2 j ps = radius2 0 ps - radius2 0 (take (j-1) ps) 

radius :: Int -> [Int] -> R
radius n ps = sqrt $ radius2 n ps

data Space4d1 a = Space4d1 { x, y, z, w :: a }
instance Functor Space4d1 where
  fmap f (Space4d1 a b c d) = Space4d1 (f a) (f b) (f c) (f d)
instance Applicative Space4d1 where
  pure f = Space4d1 f f f f
  Space4d1 a b c d <*> Space4d1 i j k l = Space4d1 (a i) (b j) (c k) (d l)

data Space4d2 a = Space4d2 { xy, xz, xw, yz, yw, zw :: a }
instance Functor Space4d2 where
  fmap f (Space4d2 a b c d e g) = Space4d2 (f a) (f b) (f c) (f d) (f e) (f g)
instance Applicative Space4d2 where
  pure f = Space4d2 f f f f f f
  Space4d2 a b c d e f <*> Space4d2 i j k l m n = Space4d2 (a i) (b j) (c k) (d l) (e m) (f n)

data Space4d3 a = Space4d3 { xyz, xyw, xzw, yzw :: a }
instance Functor Space4d3 where
  fmap f (Space4d3 a b c d) = Space4d3 (f a) (f b) (f c) (f d)
instance Applicative Space4d3 where
  pure f = Space4d3 f f f f
  Space4d3 a b c d <*> Space4d3 i j k l = Space4d3 (a i) (b j) (c k) (d l)

data Figure4 a = Figure4 a (Space4d1 a) (Space4d2 a) (Space4d3 a)
instance Functor Figure4 where
  fmap f (Figure4 o ps qs rs) = Figure4 (f o) (fmap f ps) (fmap f qs) (fmap f rs)
instance Applicative Figure4 where
  pure f = Figure4 f (pure f) (pure f) (pure f)
  Figure4 a bs cs ds <*> Figure4 i js ks ls = Figure4 (a i) (bs <*> js) (cs <*> ks) (ds <*> ls)



type F4 = Figure4 V4

initialP4 :: [V4] -> V4 -> V4
initialP4 [V4 a11 a12 a13 a14, V4 a21 a22 a23 a24, V4 a31 a32 a33 a34, V4 a41 a42 a43 a44] v =
  let m = M4 a11 a12 a13 a14 a21 a22 a23 a24 a31 a32 a33 a34 a41 a42 a43 a44
      u = norm $ inv m ^^*^ v
  in u
initialP4 _ _ = error "initialP4"

initialF4 :: [V4] -> V4 -> F4
initialF4 [l0,l1,l2,l3] p = let munge = norm . center p . subgroup in Figure4
  (norm p)
  (Space4d1
    (munge [l0])
    (munge [l1])
    (munge [l2])
    (munge [l3]))
  (Space4d2
    (munge [l0, l1])
    (munge [l0, l2])
    (munge [l0, l3])
    (munge [l1, l2])
    (munge [l1, l3])
    (munge [l2, l3]))
  (Space4d3
    (munge [l0, l1, l2])
    (munge [l0, l1, l3])
    (munge [l0, l2, l3])
    (munge [l1, l2, l3]))
initialF4 _ _ = error "initialF4"

closure :: (Approx m, M m) => [m] -> [m]
closure gs = closure' gs gs gs

closure' :: (Approx m, M m) => [m] -> [m] -> [m] -> [m]
closure' gs ms ns =
  let new = (deleteFirstsBy (~=~) . nubBy (~=~) . liftM2 (^^*^^) gs $ ns) ms
  in if null new then ms else closure' gs (ms ++ new) new

subgroup :: [V4] -> [M4]
subgroup = closure . map reflector

center :: V4 -> [M4] -> V4
center v ms = (foldr (^+^) (V4 0 0 0 0) $ map (^^*^ v) ms) ^/ (fromIntegral $ length ms)

transform :: M4 -> F4 -> F4
transform m = fmap (m ^^*^)

{-
transforms :: M4 -> [F4] -> [F4]
transforms m fs = map (m `transform`) fs
-}

polychoron :: [M4] -> F4 -> [F4]
polychoron ms f = map (`transform` f) ms

fillFigure4 :: R -> Space4d2 (Color3 R) -> Space4d2 Bool -> (Space4d3 V4 -> V4) -> F4 -> F4 -> IO ()
fillFigure4 q cs vs _ _ {- axis (Figure4 _ _ _ r0s) -} (Figure4 o ps qs _) = do
  let h = id -- if axis r0s ~=~ axis rs then fmap (1-) else id
  when (xy vs) $ face q (h $ xy cs) o (x ps) (y ps) (xy qs)
  when (xz vs) $ face q (h $ xz cs) o (x ps) (z ps) (xz qs)
  when (xw vs) $ face q (h $ xw cs) o (x ps) (w ps) (xw qs)
  when (yz vs) $ face q (h $ yz cs) o (y ps) (z ps) (yz qs)
  when (yw vs) $ face q (h $ yw cs) o (y ps) (w ps) (yw qs)
  when (zw vs) $ face q (h $ zw cs) o (z ps) (w ps) (zw qs)

{-
project :: V4 -> Maybe V3
project (V4 xx yy zz ww) =
  let k = 1 / (1 - ww)
      v3 = V3 xx yy zz ^* k
  in  if ww < 0.99 then Just v3 else Nothing
-}

face :: R -> Color3 R -> V4 -> V4 -> V4 -> V4 -> IO ()
face q c p1 p2 p3 p4 = triangle q c (p1, p2, p3) >> triangle q c (p4, p2, p3)

{-
triangle' :: Color3 R -> (V4, V4, V4) -> IO ()
triangle' c (p1, p2, p3) =
  case liftM3 (,,) (project p1) (project p2) (project p3) of
    Nothing -> return ()
    Just (v1, v2, v3) -> {- print (v1,v2,v3) >> -} draw v1 >> draw v2 >> draw v3
  where
    draw (V3 xx yy zz) = color (fmap (zz *) c) >> vertex (Vertex3 xx yy zz)
-}

triangle' :: Color3 R -> (V4, V4, V4) -> IO ()
triangle' c (p1, p2, p3) = draw p1 >> draw p2 >> draw p3
  where
    draw (V4 xx yy zz ww) = color c >> vertex (Vertex4 xx yy zz ww)

triangle :: R -> Color3 R -> (V4, V4, V4) -> IO ()
triangle q c t0@(p1, p2, p3) = do
  let e1 = p2 |-| p3
      e2 = p3 |-| p1
      e3 = p1 |-| p2
      q1 = norm $ p2 ^+^ p3
      q2 = norm $ p3 ^+^ p1
      q3 = norm $ p1 ^+^ p2
      (e, (t1, t2)) = maximumBy (comparing fst)
        [ (e1, ((p1, p2, q1), (p1, q1, p3)))
        , (e2, ((p1, p2, q2), (q2, p2, p3)))
        , (e3, ((p1, q3, p3), (q3, p2, p3)))
        ]
  if e < q then triangle' c t0 else triangle q c t1 >> triangle q c t2

fillFigures4 :: R -> Space4d2 (Color3 R) -> Space4d2 Bool -> [F4] -> IO ()
fillFigures4 _ _ _ [] = return ()
fillFigures4 q cs v fs@(f:_) = renderPrimitive Triangles $ mapM_ (fillFigure4 q cs v xyz f) fs

data State = State { sView :: Size, sColour :: Space4d2 (Color3 R), sVisibility :: Space4d2 Bool, sQuality :: R, sShape :: V4, sPolytope :: Polytope, sTransform :: M4, sMove :: V3, sFrame :: Int, sProgram :: Program }

reshape :: IORef State -> Size -> IO ()
reshape r v = atomicModifyIORef r $ \s -> (s { sView = v }, ())

timer :: Timeout -> IORef State -> IO ()
timer t r = do
  addTimerCallback t $ timer t r
  postRedisplay Nothing

data Polytope = Polytope [M4] [V4]

red :: Color3 R
red = Color3 ((0.114/0.299)**0.2) 0 0

yellow :: Color3 R
yellow = Color3 ((0.114/0.299)**0.2) ((0.114/0.587)**0.2) 0

green :: Color3 R
green = Color3 0 ((0.114/0.587)**0.2) 0

cyan :: Color3 R
cyan = Color3 0 ((0.114/0.587)**0.2) 1

blue :: Color3 R
blue  = Color3 0 0 1

magenta :: Color3 R
magenta = Color3 ((0.114/0.299)**0.2) 0 1

display :: IORef State -> IO ()
display ref = do
  s <- atomicModifyIORef ref $ \s' -> (s', s')
  let vp@(Size vw vh) = sView s
      Polytope syms fr = sPolytope s
      period = 25 * 30
      t = fromIntegral (sFrame s) / period
      shape = V4 (4 ** cos (2 * pi * 10 * t)) 1 1 1
--      mb = ball 64 $ sMove s
      co = cos (2 * pi / period)
      si = sin (2 * pi / period)
      mb = M4 1 0 0 0  0 1 0 0  0 0 co si  0 0 (-si) co
      m = mb ^^*^^ sTransform s
  viewport $= (Position 0 0, vp)
  matrixMode $= Projection
  loadIdentity
  perspective 45 (fromIntegral vw / fromIntegral vh) 1 20
  matrixMode $= Modelview 0
  loadIdentity
  multMatrix =<< (newMatrix RowMajor (matToList m) :: IO (GLmatrix R))
  clear [ColorBuffer, DepthBuffer]
  depthFunc $= Just Less
  currentProgram $= Just (sProgram s)
  uloc <- get $ uniformLocation (sProgram s) "lights"
  withArray (concat [[xx,yy,zz,-0.5::GLfloat]|xx<-[-0.5,0,0.5],yy<-[-0.5,0,0.5],zz<-[-0.5,0,0.5], even . length . filter (== 0) $ [xx,yy,zz]]) $ \p -> uniformv uloc 14 (castPtr p :: Ptr (Vertex4 GLfloat))
  let
  fillFigures4 (sQuality s) (sColour s) (sVisibility s) . polychoron syms . initialF4 fr . initialP4 fr $ shape
  currentProgram $= Nothing
  flush
  swapBuffers
  reportErrors
  atomicModifyIORef ref $ \s' -> (s' { sFrame = sFrame s' + 1, sTransform = m }, ())
--  when (t < 1) $ hSnapshot stdout (Position 0 0) vp

main :: IO ()
main = do
  hSetBuffering stdin LineBuffering
  let vp = Size 1050 576
  initialWindowSize $= vp
  initialDisplayMode $= [RGBAMode, DoubleBuffered, WithDepthBuffer]
  (_, dims) <- getArgsAndInitialize
  let [p,q,r] = map read dims
      Just (file, size) = lookup (p,q,r) [((3,3,3),("3_3_3.4d",   120))
                                         ,((4,3,3),("4_3_3.4d",   384))
                                         ,((3,4,3),("3_4_3.4d",  1152))
                                         ,((5,3,3),("5_3_3.4d", 14400))]
      n = 4*4 * size
      b = n * sizeOf (undefined::R)
  matrices <- fmap toMats $ withBinaryFile file ReadMode $ \h -> do
   allocaBytes b $ \ptr -> do
    _ <- hGetBuf h ptr b
    peekArray n ptr
  _ <- createWindow "reflex"
  prog <- shader vert frag
  let f = fr4 p q r
  sr <- newIORef $ State
    { sView = vp
    , sColour = Space4d2 yellow yellow yellow yellow yellow yellow -- magenta blue red cyan yellow green 
    , sVisibility = Space4d2 True False True False True False
    , sQuality = 0.1
    , sShape = V4 1 1 1 1
    , sPolytope = Polytope matrices f
    , sTransform = V.i -- reflector (norm $ ((f !! 0) ^-^ (f !! 1) ^+^ (f !! 2) ^-^ (f !! 3)))
    , sMove = V3 0 0 0
    , sFrame = 0
    , sProgram = prog
    }
  _ <- forkIO . forever $ commands sr
  reshapeCallback $= Just (reshape sr)
  displayCallback $= display sr
  timer 40 sr
  mainLoop

shader :: String -> String -> IO Program
shader v f = do
  prog <- createProgram
  vv <- createShader VertexShader
  ff <- createShader FragmentShader
  shaderSource vv $= [v]
  shaderSource ff $= [f]
  compileShader vv
  compileShader ff
  attachedShaders prog $= [vv, ff]
  linkProgram prog
  info <- get (programInfoLog prog)
  when (not (null info)) $ putStrLn info
  return prog

matToList :: M4 -> [R]
matToList (M4 a b c d e f g h i j k l m n o p) = a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:[]
toMats :: [R] -> [M4]
toMats (a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:zz) = M4 a b c d e f g h i j k l m n o p : toMats zz
toMats _ = []

data Toggle
  = On
  | Off
  deriving (Read, Show)

isOn :: Toggle -> Bool
isOn On = True
isOn _ = False

data Command
  = Shape R R R R
  | XY Toggle
  | XZ Toggle
  | XW Toggle
  | YZ Toggle
  | YW Toggle
  | ZW Toggle
  | Move R R R
  | Quality R
  deriving (Read, Show)

commands :: IORef State -> IO ()
commands r = do
  l <- getLine
  let l' = (if strip then {- unwords . drop 1 . words . -} filter (/=';') else id) l
  case reads l' of
    (cmd,""):_ -> {- print cmd >> -} case cmd of
      Shape a b c d -> atomicModifyIORef r $ \s -> (s { sShape = V4 a b c d }, ())
      XY v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { xy = isOn v } }, ())
      XZ v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { xz = isOn v } }, ())
      XW v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { xw = isOn v } }, ())
      YZ v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { yz = isOn v } }, ())
      YW v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { yw = isOn v } }, ())
      ZW v -> atomicModifyIORef r $ \s -> (s { sVisibility = (sVisibility s) { zw = isOn v } }, ())
      Move i j k -> atomicModifyIORef r $ \s -> (s { sMove = V3 i j k }, ())
      Quality q | q > 0 -> atomicModifyIORef r $ \s -> (s { sQuality = recip q }, ())
      _ -> return ()
    _ -> return ()
  where strip = True

vert :: String
vert = unlines
  [ "void main(void) {"
  , "  vec4 v = gl_ModelViewMatrix * gl_Vertex;"
  , "  vec3 u = v.xyz / (1.0 - v.w);"
  , "  u *= 2.0;"
  , "  u -= vec3(0.0, 0.0, 16.0);"
  , "  gl_Position = gl_ProjectionMatrix * vec4(u, 1.0);"
  , "  gl_TexCoord[0] = v;"
  , "  gl_TexCoord[1] = gl_Vertex;"
  , "  gl_FrontColor = gl_Color;"
  , "}"
  ]

frag :: String
frag = unlines
  [ "uniform vec4 lights[14];"
  , "void main(void) {"
  , "  float shine = 128.0;"
  , "  const vec3 light = vec3(0.0, 0.0, 50.0);"
  , "  vec3 c0 = gl_Color.rgb;"
  , "  vec4 j = normalize(gl_TexCoord[1]) * 16.0;"
  , "  vec4 k = j + vec4(0.5);"
  , "  j -= floor(j);"
  , "  k -= floor(k);"
  , "  if (length(k - vec4(0.5)) < 0.5) {"
  , "    shine /= 2.0;"
  , "  }"
  , "  if (length(j - vec4(0.5)) < 0.5) {"
  , "    shine *= 2.0;"
  , "  }"
  , "  vec3 p = gl_FragCoord.xyz;"
  , "  vec4 q0 = normalize(gl_TexCoord[0]);"
  , "  vec3 q = q0.xyz / (1.0 - q0.w);"
  , "  vec3 a = normalize(dFdx(q));"
  , "  vec3 b = normalize(dFdy(q));"
  , "  vec3 n = normalize(cross(a, b));"
  , "  if (n.z > 0.0) n = -n;"
  , "  vec3 c = vec3(0.0);"
  , "  for (int i = 0; i < 14; ++i) {"
  , "    vec3 l = 16.0 * lights[i].xyz;"
  , "    vec3 d = normalize(q - l);"
  , "    float dn = dot(d, n);"
  , "    if (dn > 0.0) {"
  , "      vec3 h = reflect(-d, n);"
  , "      float diffuse = max(dn, 0.0) / 12.0;"
  , "      float specular = pow(max(dot(h, n), 0.0), shine);"
  , "      c += diffuse * c0 + (c0 + vec3(15.0)) * (specular / 24.0);"
  , "    }"
  , "  }"
  , "  gl_FragColor = vec4(c / pow(max(-q.z, 1.0), 4.0), 1.0);"
  , "  gl_FragDepth = p.z;"
  , "}"
  ]
